<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome, <?= $model->username; ?>!</h1>

        <p class="lead">You are welcome to product browsing application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">View Products</a></p>
    </div>

    