<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $product_id
 * * @property int $product_name
 * @property string $sku_number
 * @property string $part_number
 * @property string $image
 * @property string $description
 * @property string $specification
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_name','sku_number', 'part_number', 'image', 'description', 'specification'], 'required'],
            [['description', 'specification','product_name'], 'string'],
            [['sku_number', 'part_number'], 'string', 'max' => 100],
            ['sku_number','match','pattern'=>'/^[a-z0-9A-Z]{10,20}$/'],
            ['part_number','match','pattern'=>'/^[a-z0-9A-Z]{10,20}$/'],
            [['image'], 'safe'],
            [['image'], 'file', 'extensions'=>'jpg , png , jpeg , gif , bmp'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_name' => 'Product Name',
            'sku_number' => 'Sku Number',
            'part_number' => 'Part Number',
            'image' => 'Image',
            'description' => 'Description',
            'specification' => 'Specification',
        ];
    }
}
