<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]);?>

    <div class='row'>
    <div class='col-md-3'>

    <?=$form->field($model, 'product_name')->textInput(['maxlength' => true, 'style' => 'text-transform:capitalize'])?>
    </div>
    <div class='col-md-3'>

    <?=$form->field($model, 'sku_number')->textInput(['maxlength' => true, 'style' => 'text-transform:uppercase'])?>
    </div>

    <div class='col-md-3'>

    <?=$form->field($model, 'part_number')->textInput(['maxlength' => true, 'style' => 'text-transform:uppercase'])?>
    </div>

    <div class='col-md-3'>

    <?=$form->field($model, 'image')->fileInput()?>
    </div>
    </div>

    <div class='row'>
    
    <div class='col-md-6'>

    <?=$form->field($model, 'description')->textarea(['rows' => 6])?>

    </div>
    <div class='col-md-6'>
    <?=$form->field($model, 'specification')->textarea(['rows' => 6])?>
    </div>

    </div>

    <div class="form-group">
        <?=Html::submitButton('Save', ['class' => 'btn btn-success'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
