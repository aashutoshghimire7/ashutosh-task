<?php

use yii\bootstrap\Html;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Welcome, <?= $model->username; ?>!</h1>

        <p class="lead">You are welcome to product browsing application.</p>

        <p><?= 'Click to view '. Html::a('Products', ['product/index']) ?></p>
    </div>

    